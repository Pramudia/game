package com.example.listgrid;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.GameViewHolder> {

    private Context context;
    private ArrayList<Game> gameArrayList;

    public ArrayList<Game> getGame(){
        return gameArrayList;
    }

    public GameAdapter(Context context){
        this.context = context;
    }

    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view, parent, false);
        return new GameViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GameViewHolder holder, int position) {
        Game game = getGame().get(position);
        holder.txtJudulGame.setText(game.getJudulGame());
        holder.txtTahunGame.setText(game.getTahunGame());
        holder.txtDescGame.setText(game.getDescGame());

        Glide.with(context)
                .load(game.getImgGame())
                .into(holder.imgGame);
    }

    @Override
    public int getItemCount(){
        return getGame().size();
    }

    public void setGame(ArrayList<Game> gameArrayList) {
        this.gameArrayList = gameArrayList;
    }

    class GameViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener{
        private ImageView imgGame;
        private TextView txtJudulGame, txtTahunGame, txtDescGame;

        GameViewHolder(final View itemView){
            super(itemView);

            imgGame = itemView.findViewById(R.id.img);
            txtJudulGame = itemView.findViewById(R.id.judul);
            txtTahunGame = itemView.findViewById(R.id.tahun);
            txtDescGame = itemView.findViewById(R.id.desc);

            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            Game game = getGame().get(position);

            game.setJudulGame(game.getJudulGame());
            game.setDescGame(game.getDescGame());

            Intent intent = new Intent(itemView.getContext(), DetailActivity.class);
            intent.putExtra(DetailActivity.EXTRA_GAME, game);
            context.startActivity(intent);

        }
    }
}
