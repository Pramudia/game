package com.example.listgrid;

import java.util.ArrayList;

public class GameData {

    public static String[][] game = new String[][]{
            {
                    String.valueOf(R.drawable.mll),
                    "Mobile Legend Bang Bang",
                    "2017",
                    "Pertarungan Strategi 5 vs 5"
            },
            {
                    String.valueOf(R.drawable.pubgg),
                    "PUBG",
                    "2017",
                    "Permainan Batle Ground"
            },
            {
                    String.valueOf(R.drawable.hg),
                    "Hago",
                    "2017",
                    "Aplikasi Dengan Berbagai Game"
            },
            {
                    String.valueOf(R.drawable.lordm),
                    "Lords Mobile",
                    "2017",
                    "Pertarungan antar Monster Dan istana lainnya"
            },
            {
                    String.valueOf(R.drawable.crisis),
                    "Crisis Action",
                    "2017",
                    "Game FPS Sudut Pandang Orang Pertama"
            },
            {
                    String.valueOf(R.drawable.aov),
                    "Garena AOV",
                    "2017",
                    "Pertempuran 5 vs 5"
            },
            {
                    String.valueOf(R.drawable.coc),
                    "Clash Of Clans",
                    "2017",
                    "Pertempuran Antar Clan"
            },
            {
                    String.valueOf(R.drawable.cr),
                    "Clash Royale",
                    "2017",
                    "Batle Deck Melawan atau Menghindar"
            },
            {
                    String.valueOf(R.drawable.bs),
                    "Brawl Stars",
                    "2017",
                    "Pertarungan 3 vs 3"
            },
            {
                    String.valueOf(R.drawable.ev),
                    "Hero Evolve",
                    "2017",
                    "Game Batle 5 vs 5"
            }
    };

    public static ArrayList<Game> getListData(){
        ArrayList<Game> games = new ArrayList<>();
        for (String[] data : game){
            Game game1 = new Game();
            game1.setImgGame(Integer.valueOf(data[0]));
            game1.setJudulGame(data[1]);
            game1.setTahunGame(data[2]);
            game1.setDescGame(data[3]);
            games.add(game1);
        }
        return games;
    }

}
