package com.example.listgrid;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.listgrid.adapter.CustomAdapter;

import java.util.ArrayList;


public class FragmentSatu extends Fragment{

    RecyclerView recyclerView;
    private ArrayList<Game> gameArrayList = new ArrayList<>();

//    private static final String TAG = "RecyclerViewFragment";
//    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
//    private static final int SPAN_COUNT = 2;
//    private static final int DATASET_COUNT = 60; // menampilkan data sebanyak value

//    private enum LayoutManagerType {
//        GRID_LAYOUT_MANAGER,
//        LINEAR_LAYOUT_MANAGER
//    }
//
//    protected LayoutManagerType mCurrentLayoutManagerType;
//
//    protected RecyclerView mRecyclerView;
//    protected CustomAdapter mAdapter;
//    protected RecyclerView.LayoutManager mLayoutManager;
//    protected String[] mDataset, mDataset2;
//    protected int[] mDataset3;
//
//    int [] img = {R.drawable.mll, R.drawable.pubgg,R.drawable.hg,R.drawable.lordm,R.drawable.crisis,R.drawable.aov,
//                    R.drawable.coc,R.drawable.cr,R.drawable.bs,R.drawable.ev};
//    String [] judul = {"Mobile Legend Bang Bang","PUBG","Hago","Lords Mobile","Crisis Action", "Garena AOV", "Clash Of Clans",
//                        "Clash Royale", "Brawl Stars", "Hero Evolve"};
//    String [] desc = {"Pertarungan Strategi 5 vs 5","Permainan Batle Ground","Aplikasi Dengan Berbagai Game", "Pertarungan antar Monster Dan istana lainnya",
//                            "Game FPS Sudut Pandang Orang Pertama", "Pertempuran 5 vs 5", "Pertempuran Antar Clan", "Batle Deck Melawan atau Menghindar",
//                            " Pertarungan 3 vs 3", "Game Batle 5 vs 5"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fragment_satu, container, false);
//        rootView.setTag(TAG);

        // BEGIN_INCLUDE(initializeRecyclerView)
//        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.

        recyclerView = rootView.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        gameArrayList.addAll(GameData.getListData());

        showRecyclerGame();

//        mLayoutManager = new LinearLayoutManager(getActivity());
//
//        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
//
//        if (savedInstanceState != null) {
//            // Restore saved layout manager type.
//            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
//                    .getSerializable(KEY_LAYOUT_MANAGER);
//        }
//        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
//
//        mAdapter = new CustomAdapter(mDataset,mDataset2,mDataset3);
//        // Set CustomAdapter as the adapter for RecyclerView.
//        mRecyclerView.setAdapter(mAdapter);
//        // END_INCLUDE(initializeRecyclerView)

        return rootView;
    }

//    /**
//     * Set RecyclerView's LayoutManager to the one given.
//     *
//     * @param layoutManagerType Type of layout manager to switch to.
//     */
//    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
//        int scrollPosition = 0;
//
//        // If a layout manager has already been set, get current scroll position.
//        if (mRecyclerView.getLayoutManager() != null) {
//            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
//                    .findFirstCompletelyVisibleItemPosition();
//        }
//
//        switch (layoutManagerType) {
//            case GRID_LAYOUT_MANAGER:
//                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
//                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
//                break;
//            case LINEAR_LAYOUT_MANAGER:
//                mLayoutManager = new LinearLayoutManager(getActivity());
//                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
//                break;
//            default:
//                mLayoutManager = new LinearLayoutManager(getActivity());
//                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
//        }
//
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.scrollToPosition(scrollPosition);
//    }

//    @Override
//    public void onSaveInstanceState(Bundle savedInstanceState) {
//        // Save currently selected layout manager.
//        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
//        super.onSaveInstanceState(savedInstanceState);
//    }
//    /**
//     * Generates Strings for RecyclerView's adapter. This data would usually come
//     * from a local content provider or remote server.
//     */
//    private void initDataset() {
//        mDataset = new String[judul.length];
//        mDataset2 = new String[desc.length];
//        mDataset3 = new int[img.length];
//        for (int i = 0; i < judul.length; i++) {
//            mDataset[i] = judul[i];
//            mDataset2[i] = desc[i];
//            mDataset3[i] = img[i];
//        }
//    }

    private void showRecyclerGame(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
        GameAdapter gameAdapter = new GameAdapter(this.getContext());
        gameAdapter.setGame(gameArrayList);
        recyclerView.setAdapter(gameAdapter);
    }

}
