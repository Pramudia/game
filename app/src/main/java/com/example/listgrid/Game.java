package com.example.listgrid;

import android.os.Parcel;
import android.os.Parcelable;

public class Game implements Parcelable {
    private Integer imgGame;
    private String judulGame;
    private String tahunGame;
    private String descGame;

    Game(int imgGame, String judulGame, String tahunGame,String descGame){
        this.imgGame = imgGame;
        this.judulGame = judulGame;
        this.tahunGame = tahunGame;
        this.descGame = descGame;
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };

    public Game() {

    }

    Integer getImgGame(){
        return imgGame;
    }

    public void setImgGame(Integer imgGame){
        this.imgGame = imgGame;
    }

    public String getJudulGame(){
        return judulGame;
    }

    public void setJudulGame(String judulGame){
        this.judulGame = judulGame;
    }

    String getTahunGame(){return tahunGame;}

    public void setTahunGame(String tahunGame){
        this.tahunGame = tahunGame;
    }

    String getDescGame(){
        return descGame;
    }

    public void setDescGame(String descGame){
        this.descGame = descGame;
    }

    protected Game(Parcel in){
        imgGame = in.readInt();
        judulGame = in.readString();
        tahunGame = in.readString();
        descGame = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(imgGame);
        parcel.writeString(judulGame);
        parcel.writeString(tahunGame);
        parcel.writeString(descGame);
    }
}
