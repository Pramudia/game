package com.example.listgrid;


import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends AppCompatActivity {

    TextView detail_judul, detail_terbit, detail_desc;
    ImageView imgGame;

    public static final String EXTRA_GAME = "EXTRA_GAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        detail_judul = findViewById(R.id.detail_judul);
        detail_terbit = findViewById(R.id.detail_terbit);
        detail_desc = findViewById(R.id.detail_desc);
        imgGame = findViewById(R.id.imgGame);

        Game game = getIntent().getParcelableExtra(EXTRA_GAME);

        assert game != null;
        detail_judul.setText(game.getJudulGame());
        detail_terbit.setText(game.getTahunGame());
        detail_desc.setText(game.getDescGame());

        Glide.with(this)
                .load(game.getImgGame())
                .into(imgGame);

    }

}
