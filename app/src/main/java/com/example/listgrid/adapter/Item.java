package com.example.listgrid.adapter;

public class Item {

    private String mName;
    private int mImg;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getImg() {
        return mImg;
    }

    public void setImg(int img) {
        this.mImg = img;
    }
}
