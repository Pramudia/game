package com.example.listgrid.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.listgrid.R;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.ViewHolder> {

    List<Item> mItems;

    public GridAdapter() {
        super();
        mItems = new ArrayList<Item>();
        Item nama = new Item();
        nama.setName("Mobile Legend Bang Bang");
        nama.setImg(R.drawable.mll);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Player Unknown's Battle Grounds");
        nama.setImg(R.drawable.pubgg);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Hago");
        nama.setImg(R.drawable.hg);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Lord Mobile");
        nama.setImg(R.drawable.lordm);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Crisis Action");
        nama.setImg(R.drawable.crisis);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Garena AOV");
        nama.setImg(R.drawable.aov);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Clash Of Clans");
        nama.setImg(R.drawable.coc);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Clash Royale");
        nama.setImg(R.drawable.cr);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Brawl Stars");
        nama.setImg(R.drawable.bs);
        mItems.add(nama);

        nama = new Item();
        nama.setName("Hero Elvoved");
        nama.setImg(R.drawable.ev);
        mItems.add(nama);



    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.grid_view, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        Item nature = mItems.get(i);
        viewHolder.tvspecies.setText(nature.getName());
        viewHolder.imgThumbnail.setImageResource(nature.getImg());

    }

    @Override
    public int getItemCount() {

        return mItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder  {


        public ImageView imgThumbnail;
        public TextView tvspecies;

        public ViewHolder(View itemView) {
            super(itemView);
            imgThumbnail = (ImageView)itemView.findViewById(R.id.img_thumbnail);
            tvspecies = (TextView)itemView.findViewById(R.id.status);

        }
    }
}
